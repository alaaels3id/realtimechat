var app 	= require('express')();
var server 	= require('http').Server(app);
var io 		= require('socket.io')(server);

users = [];
connections = [];

server.listen(process.env.PORT || 3000);
console.log('server running ...');

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {

	connections.push(socket);
	console.log('Connected: %s sockets connected', connections.length);

	socket.on('disconnect', function(){
		connections.splice(connections.indexOf(socket), 1);
		console.log('Disconnected: %s sockets connected', connections.length);
	});

	socket.on('send message', function(data){
		io.sockets.emit('new message', { msg: data });
	});
});